import React, { Component } from 'react';
import logo from './img/logo.png';
import './css/styles.css';
import FishShop from './shop/FishShop';
import Modal from './Modal';

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
		this.showModal = this.showModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	showModal(title, message, loading) {
		this.setState({
			modal: true,
			title: title,
			message: message,
			loading: loading
		});
	}
	closeModal() {
		this.setState({
			modal: false
		});
	}

	render() {
		return (
			<div className="App">

				{this.state.modal && (
					<Modal title={this.state.title} message={this.state.message} loading={this.state.loading} closeModal={this.closeModal} />
				)}

				<div className="header">
					<img src={logo} className="logo" alt="logo" />
					<h1>Fish Shop</h1>
				</div>

				<FishShop showModal={this.showModal} closeModal={this.closeModal} />

			</div>
		);
	}
}

export default App;
