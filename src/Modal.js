import React, {Component} from 'react';
import loading from './img/loading.png';

class Modal extends Component {

	render() {
		return (
			<div className="lightbox">
				<div className="modal">
					{this.props.title && (
						<h3>{this.props.title}</h3>
					)}
					{this.props.message && (
						<div>
							<p>{this.props.message}</p>
							<button onClick={this.props.closeModal}>OK</button>
						</div>
					)}
					{this.props.loading && (
						<img className="spinner" src={loading} alt="loading..." />
					)}
				</div>
			</div>
		);
	}
}

export default Modal;