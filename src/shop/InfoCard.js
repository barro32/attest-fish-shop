import React, {Component} from 'react';

class InfoCard extends Component {

	constructor(props) {
		super(props);

		this.state = {
			value: ''
		}

		this.buyFish = this.buyFish.bind(this);
		this.handleFilterInput = this.handleFilterInput.bind(this);
		this.clearInput = this.clearInput.bind(this);
	}

	buyFish() {
		const title = 'Thank You!';
		const message = 'Your fish are swimming their way to you';

		this.props.showModal(title, message, false);
	}

	handleFilterInput(e) {
		e.preventDefault();

		this.setState({
			value: e.target.value
		});
		this.props.onFilterInput(e.target.value);
	}

	clearInput(e) {
		e.preventDefault();

		this.setState({
			value: ''
		});
		this.props.onFilterInput('');
	}

	render() {
		return (
			<div className="info-card card">
				<h3 className={this.props.colour}>{this.props.message}</h3>
				<button className="btn-buy" disabled={this.props.error} onClick={this.buyFish}>Buy Fish</button>
				<div>
					<input type="text" placeholder="Filter Fish" value={this.state.value} onChange={this.handleFilterInput} />
					{this.state.value.length > 0 && (
						<button className="filterClear" onClick={this.clearInput}>x</button>
					)}
				</div>
			</div>
		);
	}
}

export default InfoCard;