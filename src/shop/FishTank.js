import React, {Component} from 'react';
import FishCard from './FishCard';

class FishTank extends Component {

	render() {
		return (
			<div className="your-tank card">
				<h2>Your Tank</h2>
				<ul className="tank">
					{this.props.fish.map((fish) =>
						<FishCard fish={fish} key={fish} selected={true} updateTank={this.props.updateTank} fakeFish={this.props.fakeFish} />
					)}
				</ul>
			</div>
		);
	}
}

export default FishTank;