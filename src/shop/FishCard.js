import React, {Component} from 'react';

class FishCard extends Component {

	constructor(props) {
		super(props);

		this.state = {
			selected: this.props.selected
		};

		this.selectFish = this.selectFish.bind(this);
	}

	selectFish(e) {
		e.preventDefault();
		
		this.props.updateTank(this.props.fish);
	}

	render() {
		var classesSelected = "fish-card selected " + this.props.hide;
		var classes = "fish-card " + this.props.hide;
		return ( 
			<li className={this.props.selected ? classesSelected : classes} onClick={this.selectFish}>
				<h4 className={this.props.fakeFish.includes(this.props.fish) ? "fake" : ""}>
					{this.props.fish.replace(/_/g, ' ')}
				</h4>
			</li>
		);
	}
}

export default FishCard;