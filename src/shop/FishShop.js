import React, {Component} from 'react';
import fishArray from './fish';
import FishCard from './FishCard';
import FishTank from './FishTank';
import InfoCard from './InfoCard';

const useModals = false;

class FishShop extends Component {

	constructor(props) {
		super(props);
		this.state = {
			selected: [],
			error: true,
			colour: 'default',
			fakeFish: [],
			message: 'Select fish to add to your tank',
			filterText: ''
		};

		this.updateTank = this.updateTank.bind(this);
		this.handleFilterInput = this.handleFilterInput.bind(this);
	}

	updateTank(fish) {
		var idx = this.state.selected.indexOf(fish);
		if(idx >= 0) {
			this.setState({
				selected: this.state.selected.filter((x,i) => i !== idx)
			}, this.checkCompatibility);
		} else {
			this.setState({
				selected: this.state.selected.concat(fish)
			}, this.checkCompatibility);
		}
	}

	handleFilterInput(filterText) {
		this.setState({
			filterText: filterText
		});
	}

	checkCompatibility() {
		this.setState({
			message: "Checking compatibility...",
			error: true,
			colour: 'default'
		});
		if(useModals) this.props.showModal(false, false, true);

		var request = new XMLHttpRequest();

		request.open('POST', 'https://fishshop.attest.tech/compatibility');
		request.setRequestHeader('Content-Type', 'application/json');

		var that = this;
		request.onreadystatechange = function() {
			if(this.readyState === 4) {
				var res = JSON.parse(this.response);

				if(this.status === 200 && res.canLiveTogether) 
					that.compatible();

				if(this.status === 200 && !res.canLiveTogether) 
					that.incompatible();

				if(this.status === 400) 
					that.unrecognisedFish(res);

				if(this.status === 500) 
					that.shopFlooded(res);
			}
		};

		request.send(JSON.stringify({'fish': this.state.selected}));
	}

	compatible() {
		if(this.state.selected.length) {
			this.setState({
				message: 'All the fish in your tank are compatible',
				error: false,
				colour: 'green'
			});
		} else {
			this.setState({
				message: 'Select fish to add to your tank',
				error: true,
				colour: 'default'
			});
		}
		if(useModals) this.props.closeModal();
	}

	incompatible() {
		this.setState({
			message: 'The fish in your tank are not compatible, remove one to continue',
			error: true,
			colour: 'red'
		});
		if(useModals) this.props.showModal('Warning', 'The fish in your tank are not compatible', false);
	}

	unrecognisedFish(res) {
		var fake = res.errorMessage.match("'(.*)'")[1];
		var fishName = fake.replace(/_/g, ' ').replace(/(^| )(\w)/g, s => s.toUpperCase());
		this.setState({
			message:  fishName + ' is not a recognised fish, please remove it',
			error: true,
			fakeFish: this.state.fakeFish.concat(fake),
			colour: 'red'
		});
		if(useModals) this.props.showModal('Error', res.errorMessage, false);
	}

	shopFlooded(res) {
		this.setState({
			message: res.errorMessage,
			error: true,
			colour: 'red'
		});
		this.checkCompatibility();
	}


	render() {
		return (
			<div className='fish-shop'>
				<FishTank 
					fish={this.state.selected} 
					updateTank={this.updateTank} 
					fakeFish={this.state.fakeFish}
				/>
				<InfoCard 
					message={this.state.message} 
					error={this.state.error} 
					colour={this.state.colour} 
					showModal={this.props.showModal} 
					onFilterInput={this.handleFilterInput}
				/>
				<ul>
					{fishArray.map((fish) =>
						<FishCard 
							hide={fish.replace(/_/g, ' ').indexOf(this.state.filterText.toLowerCase()) >= 0 ? '' : 'hide'} 
							fish={fish} key={fish} 
							selected={this.state.selected.includes(fish)} 
							updateTank={this.updateTank} 
							fakeFish={this.state.fakeFish}
						/>
					)}
				</ul>
			</div>
		);
	}
}

export default FishShop;