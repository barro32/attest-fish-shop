# Fish Shop
Created by Daniel Barrington

## About
Fish shop app built with React. You can select the fish you want and the API will tell you if they are compatible or not.

## Libraries, packages, etc.
* [React](https://facebook.github.io/react/)
* JSX / ES6 (compiled with [Babel](https://babeljs.io/))
* [Create React App](https://github.com/facebookincubator/create-react-app)
* [Gulp](http://gulpjs.com/)
* [Sass](http://sass-lang.com/)

## To run dev version
    npm install -g create-react-app
    cd fish-shop/
    npm install
    npm start
Then open [http://localhost:3000/](http://localhost:3000/)  

## Or serve the production version
    npm install -g serve
    serve -s build
Then open [http://localhost:5000/](http://localhost:5000/)